%{
#include "Reactions/ReactionLibraryBase.hpp"
#include "Reactions/FFNReactionLibrary.hpp"
%}

%include "Reactions/ReactionLibraryBase.hpp"
%include "Reactions/FFNReactionLibrary.hpp"
